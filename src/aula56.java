public class aula56 {  // for each  - calculo de media

    public static void main(String[] args) {

        int[] nums = new int[10]; // array nums, nova instancia 10 elementos
        nums[0] = 5; // elemento = valor
        nums[1] = 5; // elemento = valor
        nums[2] = 5; // elemento = valor
        nums[3] = 5; // elemento = valor
        nums[4] = 5; // elemento = valor
        nums[5] = 10; // elemento = valor
        nums[6] = 10; // elemento = valor
        nums[7] = 10; // elemento = valor
        nums[8] = 10; // elemento = valor
        nums[9] = 10; // elemento = valor

        // imprime a média aritmética
        int soma = 0; // variavel soma igual 0
        for(int item : nums){ // bloco de instrução dentro do looping, variavel item, cada ciclo de nums
            soma += item; // variavel soma mais igual item
        }

        float media = (float)soma / nums.length;  // variavel media tipo float igual soma divido por nums lenght

        System.out.println("A média aritmética é: "+ media); // imprime a média

        //imprime o menor valor da lista
        int menor = 0; // variavel menor igual 0
        int maior = 0; // variavel maior igual 0
        for(int item : nums){// bloco de instrução dentro do looping, variavel item, cada ciclo de nums
            if(item < menor || menor == 0) // se o valor de item for menor || ou menor for igual a 0
                menor = item; // menor recebe o valor de item
            if (item > maior)  //  se o valor de item for maior
                maior = item; // maior recebe o valor de item
        }

        System.out.println("O menor valor da lista é: "+menor); // imprime o menor valor
        System.out.println("O maior valor da lista é: "+maior); // imprime o maior valor

    }
}
